/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uvigo.ephyslab.aerocualreader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.System.exit;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This programme is designed with the objective to read a specified file each
 * 30 seconds. Initially, the programme wait 10 seconds before start reading the
 * file.
 *
 * @author Michael García Rodríguez
 */
public class Main implements Runnable {

    /**
     * The maximum value that the OZONE can obtain.
     */
    private static final double MAX_OZONE =  - 1.0;

    /**
     * The path of the file that contains the information to check.
     */
    private static final String FILEPATH = "."+ File.separator +"AQLMonitor.log";

    /**
     * The Thread that it will be executed in background.
     */
    private static ScheduledExecutorService scheduler;

    public static void main(String[] args) {

        File f;

        f = new File(FILEPATH);

        if (f.exists() && !f.isDirectory()) {

            scheduler = Executors.newSingleThreadScheduledExecutor();

            Runnable task = new Main();
            int initialDelay = 10;
            int periodicDelay = 30;
            scheduler.scheduleAtFixedRate(task, initialDelay, periodicDelay,
                    TimeUnit.SECONDS
            );

        } else {
            System.out.println("The path selected does not contains AQLMonitor.txt files.");
            exit(-1);
        }

        /*BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please, put the file path:");
        filePath = br.readLine();
              
        
        readFile(filePath);*/
    }

    /**
     * The tasks that the thread do.
     */
    @Override
    public void run() {
        BufferedReader br = null;

        try {

            Date d = new Date();
            System.out.println("------------------------------------");
            System.out.println("   "+d.toString());
            System.out.println("------------------------------------");
            br = new BufferedReader(new FileReader(FILEPATH));

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                if (sCurrentLine.contains("The value:")) {
                    String[] parts = sCurrentLine.split(":");
                    double value = Double.parseDouble(parts[(parts.length - 1)]);

                    
                    /**
                     * open valve when the level of ozone is down.
                     */
                    if (value > MAX_OZONE) {
                        System.out.println(value);

                        /**new Thread(new Runnable() {
                            public void run() {
                                // code goes here.
                            }
                        }).start();
                        **/
                    }
                }
            }

            

        } catch (IOException e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {

            try {

                br.close();

            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
}
