/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uvigo.ephyslab.aerocualreader;

import java.util.Arrays;
import java.util.List;
import javax.usb.UsbClaimException;
import javax.usb.UsbConfiguration;
import javax.usb.UsbDevice;
import javax.usb.UsbDeviceDescriptor;
import javax.usb.UsbDisconnectedException;
import javax.usb.UsbEndpoint;
import javax.usb.UsbException;
import javax.usb.UsbHostManager;
import javax.usb.UsbHub;
import javax.usb.UsbInterface;
import javax.usb.UsbNotActiveException;
import javax.usb.UsbNotClaimedException;
import javax.usb.UsbNotOpenException;
import javax.usb.UsbPipe;
import javax.usb.UsbServices;
import javax.usb.event.UsbPipeDataEvent;
import javax.usb.event.UsbPipeErrorEvent;
import javax.usb.event.UsbPipeListener;

/**
 *
 * @author miki
 */

public class Usb4JavaHigh {
	
	
	public UsbDevice getUsbRootHoob() {

		try {
			final UsbServices services = UsbHostManager.getUsbServices();
			return services.getRootUsbHub();
		} catch (SecurityException | UsbException e) {
                    
		}

		return null;
	}


	public UsbDevice findDevice(short vendorId, short productId) {

		return findDevice((UsbHub) getUsbRootHoob(), vendorId, productId);
	}


	public UsbDevice findDevice(UsbHub hub, short vendorId, short productId) {

		for (UsbDevice device : (List<UsbDevice>) hub.getAttachedUsbDevices()) {
			UsbDeviceDescriptor desc;
                    desc = device.getUsbDeviceDescriptor();
			if (desc.idVendor() == vendorId && desc.idProduct() == productId)
				return device;
			if (device.isUsbHub()) {
				device = findDevice((UsbHub) device, vendorId, productId);
				if (device != null)
					return device;
			}
		}
		return null;
	}
	

	
	public void readMessage(UsbInterface iface, 
			int endPoint){
		
		UsbPipe pipe = null;

		try {
			iface.claim((UsbInterface usbInterface) -> true);

			UsbEndpoint endpoint = (UsbEndpoint) iface.getUsbEndpoints().get(endPoint); // there can be more 1,2,3..
			pipe = endpoint.getUsbPipe();
			pipe.open();
			
		    byte[] data = new byte[8];
		    int received = pipe.syncSubmit(data);
		    System.out.println(received + " bytes received");

			pipe.close();

		} catch (IllegalArgumentException | UsbDisconnectedException | UsbException | UsbNotActiveException | UsbNotClaimedException | UsbNotOpenException ex) {
		} finally {
			try {
				iface.release();
			} catch (UsbClaimException e) {
			} catch (UsbNotActiveException | UsbDisconnectedException | UsbException e) {
			}
		}
		
	}

	public void readMessageAsynch(UsbInterface iface, 
			int endPoint){

		UsbPipe pipe = null;

		try {
			iface.claim((UsbInterface usbInterface) -> true);

			UsbEndpoint endpoint = (UsbEndpoint) iface.getUsbEndpoints().get(endPoint); // there can be more 1,2,3..
			pipe = endpoint.getUsbPipe();

			pipe.open();

			pipe.addUsbPipeListener(new UsbPipeListener()
			{            
				@Override
				public void errorEventOccurred(UsbPipeErrorEvent event)
				{
					UsbException error = event.getUsbException();
				}

				@Override
				public void dataEventOccurred(UsbPipeDataEvent event)
				{
					byte[] data = event.getData();

					System.out.println(Arrays.toString(data) + " bytes received");
				}
			});
			
//			pipe.close();

		} catch (UsbDisconnectedException | UsbException | UsbNotActiveException | UsbNotClaimedException ex) {
		} finally {
			try {
				iface.release();
			} catch (UsbClaimException e) {
			} catch (UsbNotActiveException | UsbDisconnectedException | UsbException e) {
			}
		}

	}


	public UsbInterface getDeviceInterface(UsbDevice device, int index) {

		UsbConfiguration configuration = device.getActiveUsbConfiguration();
		UsbInterface iface = (UsbInterface) configuration.getUsbInterfaces().get(index); // there can be more 1,2,3..

		return iface;
	}


	public void sendBulkMessage(UsbInterface iface, String message,
			int index) {

		UsbPipe pipe = null;

		try {
			iface.claim((UsbInterface usbInterface) -> true);

			UsbEndpoint endpoint = (UsbEndpoint) iface.getUsbEndpoints().get(index);
			pipe = endpoint.getUsbPipe();
			pipe.open();

			byte[] initEP = new byte[] { 0x1b, '@' };
			byte[] cutP = new byte[] { 0x1d, 'V', 1 };

			String str = "nnnnnnnnn";

			pipe.syncSubmit(initEP);
			int sent = pipe.syncSubmit(message.getBytes());
			pipe.syncSubmit(str.getBytes());
			pipe.syncSubmit(cutP);

			System.out.println(sent + " bytes sent");
			pipe.close();

		} catch (IllegalArgumentException | UsbDisconnectedException | UsbException | UsbNotActiveException | UsbNotClaimedException | UsbNotOpenException ex) {
		} finally {
			try {
				iface.release();
			} catch (UsbClaimException e) {
			} catch (UsbNotActiveException | UsbDisconnectedException | UsbException e) {
			}
		}
	}
}

